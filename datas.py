from typing import List, Union, Tuple, Optional

import torch
import torchtext
from torchtext.data import TabularDataset
from torchtext.vocab import Vectors, Vocab
from spacy.lang.zh import Chinese
from spacy.lang.en import English

from hacks import HackedField


chinese_tokenizer = Chinese()
english_tokenizer = English()


class SpecialTokens:
    start_token = "<START>"
    end_token = "<END>"
    pad_token = "<PAD>"
    unknown_token = "<UNK>"
    error_token = "<ERR>"


def padding_mask(seq: torch.Tensor, field: torchtext.data.Field) -> torch.Tensor:
    """
    :param seq: (batch_size, seq_size)
    :param field:
    :note: auto-detect device using seq.device
    :return: (batch_size, seq_size)
    """
    batch_size, seq_size = seq.shape[0], seq.shape[1]
    mask = torch.BoolTensor(batch_size, seq_size).to(seq.device)
    pad_token_repr = field.vocab.stoi[SpecialTokens.pad_token]
    for i in range(batch_size):
        for j in range(seq_size):
            mask[i][j] = pad_token_repr == seq[i][j]
    return mask


def pad(seq: List[int], vocab: Vocab, seq_size: int) -> List[int]:
    new_seq = []
    for token in seq:
        new_seq.append(token)
    for _ in range(seq_size - len(seq)):
        new_seq.append(stoi(vocab, SpecialTokens.pad_token))
    return new_seq


def itos(vocab: Vocab, idx: int) -> str:
    try:
        token = vocab.itos[idx]
    except KeyError:
        token = SpecialTokens.unknown_token
    return token


def stoi(vocab: Vocab, token: str) -> int:
    try:
        idx = vocab.stoi[token]
    except KeyError:
        idx = vocab.stoi[SpecialTokens.unknown_token]
    return idx


def untokenize_en(seq: List[str]) -> str:
    return " ".join(seq)


def untokenize_cz(seq: List[str]) -> str:
    return "".join(seq)


def tokenize_en(seq: str) -> List[str]:
    return [token.text for token in english_tokenizer(seq)]


def numericalize_en(seq: str, vocab: Vocab) -> List[int]:
    return [stoi(vocab, token) for token in tokenize_en(seq)]


def tokenize_cz(seq: str) -> List[str]:
    return [token.text for token in chinese_tokenizer(seq)]


def load_datasets(
    path: str,
    en_vectors_path: str,
    cz_vectors_path: str,
    split: bool = True,
    source_field_path: Optional[str] = None,
    target_field_path: Optional[str] = None,
) -> Union[Tuple[TabularDataset], TabularDataset]:
    """
    :param path:
    :param seq_size:
    :param en_vector_path:
    :param cz_vector_path:
    :param split: if True, will return (train_set, val_set, test_set).
        if False, will return only a dataset
    :param source_field_path:
    :param target_field_path:
    :return: 3 TabularDataset or 1, depending on split
    """
    if source_field_path is None:
        src_field = HackedField(
            tokenize=tokenize_en,
            init_token=SpecialTokens.start_token,
            eos_token=SpecialTokens.end_token,
            pad_token=SpecialTokens.pad_token,
            unk_token=SpecialTokens.unknown_token,
            batch_first=True,
        )
    else:
        src_field = torch.load(source_field_path)
    if target_field_path is None:
        target_field = HackedField(
            tokenize=tokenize_cz,
            init_token=SpecialTokens.start_token,
            eos_token=SpecialTokens.end_token,
            pad_token=SpecialTokens.pad_token,
            unk_token=SpecialTokens.unknown_token,
            batch_first=True,
        )
    else:
        target_field = torch.load(target_field_path)

    en_vectors = Vectors(en_vectors_path)
    cz_vectors = Vectors(cz_vectors_path)

    if split:
        train_set, test_set, val_set = torchtext.data.TabularDataset(
            path, format="tsv", fields=[("source", src_field), ("target", target_field)]
        ).split([0.7, 0.15, 0.15])

        if source_field_path is None:
            src_field.build_vocab(train_set, vectors=en_vectors)
        if target_field_path is None:
            target_field.build_vocab(train_set, vectors=cz_vectors)

        return train_set, test_set, val_set

    dataset = torchtext.data.TabularDataset(
        path, format="tsv", fields=[("source", src_field), ("target", target_field)]
    )

    if source_field_path is None:
        src_field.build_vocab(dataset, vectors=en_vectors)
    if target_field_path is None:
        target_field.build_vocab(dataset, vectors=cz_vectors)

    return dataset

