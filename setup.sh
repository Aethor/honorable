#!/usr/bin/bash

# install python dependencies
echo 'installig necessary python packages'
pip3 install -r requirements.txt

# download spacy english model
echo 'installing spacy english model packages'
python3 -m spacy download "en_core_web_sm"

# download english and chinese fasttext word vectors
echo '[warning] This script is about to download chinese and english fasttext
 word vectors. Each compressed file is about 1.2Go, and 4.5Go uncompressed
 (4.5Go x 2 = 9Go). please make sure that you have enough space on your hard
 drive !'
echo 'press [ENTER] to continue...'
read
cd './datas'
wget 'https://dl.fbaipublicfiles.com/fasttext/vectors-crawl/cc.en.300.vec.gz'
wget 'https://dl.fbaipublicfiles.com/fasttext/vectors-crawl/cc.zh.300.vec.gz'
gunzip -v './cc.en.300.vec.gz'
gunzip -v './cc.zh.300.vec.gz'