---
title: 'Transformation : using a TRANSFORMer for neural machine translATION'
author: Arthur Amalvy
---

\newpage

# Introduction 

Neural Machine Translation is a blooming field. Recently, latest progress in NLP neural architectures are allowing NMT to reach new highs. Among those progress, the transformer architecture (described in the now famous *Attention Is All You Need* paper (*Vaswani et al., 2017*)), making use of self-attention to solve some of the inherents problems of Recurrent Neural Networks, is certainly one of the most important.

Following our last experiment where we learned how to use BERT (see https://gitlab.com/Aethor/transformner), we wanted to learn more about the base transformer model by applying it to a neural translation problem (here, english to chinese).


# Quick start

On linux, simply running `setup.sh` should be enough to setup everything you need. be sure to activate a python virtual environnement before doing so though, as the script will use pip to install some python packages.

If the script won't work (very likely if you are on windows !) :

1. Run `pip install -r requirements.txt` to download required python packages
2. Install spacy english model (en_core_web_sm) using `python -m spacy download en_core_web_sm` or by your all means
3. Download pre-trained english and chinese word vectors from https://fasttext.cc/docs/en/crawl-vectors.html and place them under *./datas* as *cc.en.300.vec.pt* and *cc.zh.300.vec.py*

currently, no script exist to download a dataset. However, any `train.tsv` placed in the *datas* folder with first column consisting of english sentences and second column consisting of chinese sentences will work for training. the `test-sample.tsv` file is used when using the `--submit` flag.

You can now run the main script to train or run a model.

* `python main.py --train` train the model with the default configuration (see `configs/default-config.json`). The model will be saved in *out/\[training_run_id\]/*
* `python main.py --submit [model_path]` print each sentence of the training set with the proposed translation
* `python main.py --interactive [model_path]` launch an interactive translation console using the specified model
* use the `-cf` flag to specify a custom configuration flag
* `python main.py --help` will display the help


# Transformer principles

![The transformer architecture, from Vaswani et al. 2017](./assets/transformer.png)

A transformer is composed of an encoding part, formed by stacked encoders, and a decoding part, formed by stacked decoders. Each encoder is formed by a self-attention layer and a feedforward layer, and gives its output to the following encoder and its corresponding decoder. Meanwhile, decoders operate on the same principles, except they add an encoder-decoder attention layer in-between self-attention and feedforward layers. This encoder-decoder attention layer takes input from the decoder's corresponding encoder allowing it to perform attention on the source sequence.

The self-attentions layers contains some *attentions heads*, each containing 3 matrices : a *query* matrix $W_Q$, a *key* matrix $W_K$ and a *value* matrix $W_V$. For a given input vector $x_i$ (representing a token), the output vector $z_i$ calculated by a self-attention layer is :

$$z_i = \frac{softmax_{j}(q_i k_j)}{\sqrt{d_k}} v_i$$

where :

* $q_i$ is the query vector associated to $x_i$, computed using $W_Q x_i$
* $k_j$ is the key vector associated to $x_j$, computed using $W_K x_j$
* $v_i$ is the value vector associated to $x_i$, computed using $W_V x_i$
* $d_k$ is simply the dimension of the key vector. It is used to avoid having a huge $q_i k_j$ dot product, that may lead the softmax gradient to be small

As each attention head has different query, key and values matrices, they may each learn different types of structures in sequences.

In contrast to RNN, the transformer model inherently can't understand the structure of its input, because it has no notion of its order. A solution to this problem, as explained in the original paper, is to directly include this notion of order in the input. This is done by adding to the input vectors a sinusoidal function depending on the position of the input tokens (see our *PositionalEncoding* module).

For more details, you can refer to the excellent [**Illustrated Transfomer**](http://jalammar.github.io/illustrated-transformer/), Stanford's [**Annotated Transformer**](https://nlp.seas.harvard.edu/2018/04/03/attention.html) or the [**Attention Is All You Need**](https://arxiv.org/abs/1706.03762) paper.


# Model

![the model architecture](./assets/architecture.png)

We use the *torch.nn.Transformer* class as our transformer. Its input consists of pretrained FastText vectors, that we finetune during training, with added positional informations.

At the end of our pipeline, we use the output of the transformer as input for a fully connected layer. The output of this last layer is then passed through a softmax activation function, whose role is to produce a probability distribution over the output vocabulary.


# Training

We train our model for 40 epochs, with a batch size of 4 and 6 attention heads.

For the learning rate, we decided to use a *cyclic learning rate* (see https://pytorch.org/docs/stable/optim.html#torch.optim.lr_scheduler.CyclicLR) varying between 8e-5 and 1e-4. We made this choice because we observed that using low learning rate seemed to let our training get stuck in local minimas, while using higher learning rates resulted in non-optimal solutions.

To reduce the training time on our already tired RTX 2060 super without hurting our model performance too much, we apply a variation of a technique described in *Zhang et al, 2017*. The general idea is to train less on sentences which are easier to translate. Here we simply measure this "easyness" for each batch as the batch loss. Each epoch, we blacklist 20% of all examples that had the lowest loss last epoch, and we don't use them when training. At the end of the epoch, we clear the blacklist and start again. This effectively means (ecept at the first epoch) that we always train using only 80% of the dataset, and that we attend to easier batches two times less.


# Inference

At each step of the algorithm, we feed our model the source sentence, and the token he already predicted for the target sentence (in the first step, we feed him the special *<START>* token). The model then gives us a probability distribution of the next target word over the target vocabulary.

We use a beam search of size 2 to try to find good translations without a large cost. As the model has a tendency to repeat punctuations signes in the end of sentences, we also try to detect thoses cases to stop beams early.


# Results

Sadly our model simply did not have time to completely converge (the final loss being around 2.4 after 17h of training). However, we can still see that the results are going in the good direction : The translations are usually not perfect, but capture a lot of elements from the inputs. We think that by training one more day, our model could have achieved great performance.


# References

* Dakun Zhang, Jungi Kim, Josep Crego, Jean Senellart. 2017. Boosting Neural Machine Translation. ACL 2017.
* Ashish  Vaswani,  Noam  Shazeer,  Niki  Parmar,  Jakob Uszkoreit,  Llion  Jones,  Aidan  N.  Gomez,  LukaszKaiser, and Illia Polosukhin. 2017.  Attention Is All You Need.
* Alexander M. Rush. 2018. The Annotated Transformer. ACL 2017.