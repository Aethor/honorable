import torch

from hacks import _generate_square_subsequent_mask


class PositionalEncoding(torch.nn.Module):
    def __init__(self, max_seq_size: int, emb_size: int):
        super(PositionalEncoding, self).__init__()

        self.encoding_matrix = torch.zeros(max_seq_size, emb_size, requires_grad=False)
        pos = torch.arange(0, max_seq_size, dtype=torch.float).unsqueeze(1)
        divider = 1 / (
            10000 ** (torch.arange(0, emb_size, 2, dtype=torch.float) / emb_size)
        )
        self.encoding_matrix[:, 0::2] = torch.sin(pos * divider)
        self.encoding_matrix[:, 1::2] = torch.cos(pos * divider)

    def forward(self, X: torch.Tensor) -> torch.Tensor:
        """
        :param X: (batch_size, seq_size, emb_size)
        :return:  (batch_size, seq_size, emb_size)
        """
        return X + self.encoding_matrix.to(X.device)[: X.shape[1], :]


class Translator(torch.nn.Module):
    def __init__(
        self,
        en_embedding: torch.Tensor,
        cz_embedding: torch.Tensor,
        max_seq_size: int,
        emb_size: int,
        heads_nb: int,
    ):
        """
        :param base_embedding: (vocab_size, token_size)
        """
        super(Translator, self).__init__()
        self.emb_size = emb_size
        self.max_seq_size = max_seq_size
        self.cz_vocab_size = cz_embedding.shape[0]

        self.en_embedding = torch.nn.Embedding.from_pretrained(en_embedding)
        self.cz_embedding = torch.nn.Embedding.from_pretrained(cz_embedding)
        self.pos_encoding = PositionalEncoding(max_seq_size, emb_size)
        self.transformer = torch.nn.Transformer(d_model=emb_size, nhead=heads_nb)
        self.linear = torch.nn.Linear(emb_size, self.cz_vocab_size)

    def forward(
        self,
        source: torch.Tensor,
        target: torch.Tensor,
        source_padding_mask: torch.Tensor,
        target_padding_mask: torch.Tensor,
    ) -> torch.Tensor:
        """
        :param source:              (batch_size, src_seq_size)
        :param target:              (batch_size, tgt_seq_size)
        :param source_padding_mask: (batch_size, src_seq_size)
        :param target_padding_mask: (batch_size, tgt_seq_size)
        :return:                    (batch_size, cz_vocab_size, tgt_seq_size)
        """
        # (batch_size, src_seq_size, emb_size)
        source = self.en_embedding(source)
        source = self.pos_encoding(source)

        # (batch_size, tgt_seq_size, emb_size)
        target = self.cz_embedding(target)
        target = self.pos_encoding(target)

        # (src_seq_size, batch_size, emb_size)
        source = source.transpose(0, 1)
        # (tgt_seq_size, batch_size, emb_size)
        target = target.transpose(0, 1)

        # (batch_size, tgt_seq_size, emb_size)
        transformed = self.transformer(
            source,  # (src_seq_size, batch_size, emb_size)
            target,  # (tgt_seq_size, batch_size, emb_size)
            tgt_mask=_generate_square_subsequent_mask(len(target), len(target)).to(
                target.device
            ),
            src_key_padding_mask=source_padding_mask,
            tgt_key_padding_mask=target_padding_mask,
            memory_key_padding_mask=source_padding_mask,
        ).transpose(0, 1)
        # (batch_size, cz_vocab_size, tgt_seq_size)
        return self.linear(transformed).transpose(1, 2)

