import six
import torch
import torchtext


# FIXME: picked from pytorch github because of a bug in pytorch 1.3
# see https://github.com/pytorch/examples/issues/649
def _generate_square_subsequent_mask(sz1, sz2):
    mask = (torch.triu(torch.ones(sz2, sz1)) == 1).transpose(0, 1)
    mask = mask.float()
    mask = mask.masked_fill(mask == 0, float("-inf")).masked_fill(mask == 1, float(0.0))
    return mask


# FIXME: torchtext monkeypatching
class HackedField(torchtext.data.Field):
    def __init__(self, **kwargs):
        super(HackedField, self).__init__(**kwargs)

    def numericalize(self, arr, device=None):
        if self.include_lengths and not isinstance(arr, tuple):
            raise ValueError(
                "Field has include_lengths set to True, but "
                "input data is not a tuple of "
                "(data batch, batch lengths)."
            )
        if isinstance(arr, tuple):
            arr, lengths = arr
            lengths = torch.tensor(lengths, dtype=self.dtype, device=device)

        if self.use_vocab:
            if self.sequential:
                arr = [
                    [
                        self.vocab.stoi[x]
                        if x in self.vocab.stoi
                        else self.vocab.stoi[self.unk_token]
                        for x in ex
                    ]
                    for ex in arr
                ]
            else:
                arr = [
                    self.vocab.stoi[x]
                    if x in self.vocab.stoi
                    else self.vocab.stoi[self.unk_token]
                    for x in arr
                ]

            if self.postprocessing is not None:
                arr = self.postprocessing(arr, self.vocab)
        else:
            if self.dtype not in self.dtypes:
                raise ValueError(
                    "Specified Field dtype {} can not be used with "
                    "use_vocab=False because we do not know how to numericalize it. "
                    "Please raise an issue at "
                    "https://github.com/pytorch/text/issues".format(self.dtype)
                )
            numericalization_func = self.dtypes[self.dtype]
            if not self.sequential:
                arr = [
                    numericalization_func(x) if isinstance(x, six.string_types) else x
                    for x in arr
                ]
            if self.postprocessing is not None:
                arr = self.postprocessing(arr, None)

        var = torch.tensor(arr, dtype=self.dtype, device=device)

        if self.sequential and not self.batch_first:
            var.t_()
        if self.sequential:
            var = var.contiguous()

        if self.include_lengths:
            return var, lengths
        return var
