from typing import Generator, Tuple, List
from datetime import datetime
from collections import namedtuple, Counter
import argparse
import os
import math
import random
import sys

import torch
from torch.distributions import Categorical
import torchtext
from torchtext.data import Dataset, BucketIterator, Field
from tqdm import tqdm

from datas import (
    load_datasets,
    padding_mask,
    pad,
    SpecialTokens,
    untokenize_cz,
    untokenize_en,
    numericalize_en,
    itos,
    stoi,
)
from config import Config
from model import Translator


def predict(
    source: torch.Tensor,
    model: Translator,
    source_field: Field,
    out_field: Field,
    device: torch.device,
    beam_size: int,
) -> str:
    """
    :param source: the numerized sentence 
        (src_seq_size)
    :param model:
    :param source_field:
    :param out_field:
    :param device: 
    :param beam_size: number of beams in the beam search
    :return: 
    """
    if source.shape[0] > model.max_seq_size:
        return SpecialTokens.error_token

    model.to(device)
    model.eval()

    with torch.no_grad():

        Beam = namedtuple("Beam", ["translation", "score"])
        beams = [
            Beam(
                translation=[stoi(out_field.vocab, SpecialTokens.start_token)], score=0
            )
            for _ in range(beam_size)
        ]

        source = source.unsqueeze(0).to(device)
        source_padding_mask = padding_mask(source, source_field)

        def is_beam_stale(beam: Beam):
            if len(beam.translation) >= 4:
                commons = Counter(beam.translation).most_common()
                if commons[0][1] >= 3:
                    return True
                elif commons[0][1] >= 2 and commons[1][1] >= 2:
                    return True
            return False

        def is_beam_finished(beam: Beam):
            return (
                beam.translation[-1] == stoi(out_field.vocab, SpecialTokens.end_token)
                or beam.translation[-1]
                == stoi(out_field.vocab, SpecialTokens.pad_token)
                or is_beam_stale(beam)
                or len(beam.translation) == model.max_seq_size
            )

        def all_beams_finished(beams: List[Beam]):
            for beam in beams:
                if not is_beam_finished(beam):
                    return False
            return True

        while not all_beams_finished(beams):

            new_beams: List[Beam] = []
            for cur_beam in beams:

                if is_beam_finished(cur_beam):
                    continue

                out_tensor = torch.tensor([cur_beam.translation]).to(device)
                out_padding_mask = padding_mask(out_tensor, out_field)

                pred = torch.softmax(
                    model.forward(
                        source, out_tensor, source_padding_mask, out_padding_mask
                    ),
                    dim=1,
                )
                top_choices = torch.topk(pred[0, :, -1], beam_size)
                for index, score in zip(top_choices.indices, top_choices.values):
                    new_beams.append(
                        Beam(
                            translation=cur_beam.translation + [index.item()],
                            score=cur_beam.score * score.item(),
                        )
                    )

            beams = sorted(new_beams, key=lambda b: b.score)[:beam_size]
    return untokenize_cz(
        [
            itos(out_field.vocab, idx)
            for idx in max(beams, key=lambda b: b.score).translation[1:-1]
        ]
    )


def dataset_predict(
    model: Translator, dataset: Dataset, device: torch.device, batch_size: int
) -> Generator[Tuple[str], None, None]:
    model.to(device)
    model.eval()

    batches_progress = BucketIterator(
        dataset=dataset, batch_size=batch_size, device=device
    )

    with torch.no_grad():
        # batch.source (batch_size, src_seq_size)
        for batch in batches_progress:
            batch_size = batch.source.shape[0]
            src_seq_size = batch.source.shape[1]

            if src_seq_size > model.max_seq_size:
                print("[debug] skipped a batch", file=sys.stderr)
                print(f"source shape : {batch.source.shape}", file=sys.stderr)
                for i in range(batch_size):
                    yield (
                        untokenize_en(
                            [
                                itos(
                                    dataset.fields["source"].vocab,
                                    batch.source[i][j].item(),
                                )
                                for j in range(src_seq_size)
                            ]
                        ),
                        "<START><ERR><END>",
                    )
                continue

            for i in range(batch_size):
                yield (
                    untokenize_en(
                        [
                            itos(
                                dataset.fields["source"].vocab,
                                batch.source[i][j].item(),
                            )
                            for j in range(src_seq_size)
                        ]
                    ),
                    predict(
                        batch.source[i],
                        model,
                        dataset.fields["source"],
                        dataset.fields["target"],
                        device,
                        2,  # TODO: hardcoded beam size
                    ),
                )


def train_(
    model: Translator,
    dataset: Dataset,
    device: torch.device,
    loss_fn,
    optimizer,
    scheduler,
    epochs_nb: int,
    batch_size: int,
    model_dir: str,
):
    model.to(device)
    model.train()

    blacklisted_indices = set()
    max_blacklisted_len = int(0.2 * len(dataset) / batch_size)
    tqdm.write(f"[debug] max blacklisted occurences : {max_blacklisted_len}")

    for epoch in range(epochs_nb):

        losses = []

        batches_progress = tqdm(
            BucketIterator(
                dataset=dataset, batch_size=batch_size, device=device, shuffle=False
            )
        )

        # batch.source (batch_size, src_seq_size)
        # batch.target (batch_size, tgt_seq_size)
        for batch_idx, batch in enumerate(batches_progress):

            if batch_idx in blacklisted_indices:
                continue

            optimizer.zero_grad()

            if (
                batch.source.shape[1] > model.max_seq_size
                or batch.target.shape[1] > model.max_seq_size
            ):
                tqdm.write(f"Skipped batch (target shape : {batch.target.shape})")
                continue

            y = batch.target[:, 1:]
            batch.target = batch.target[:, :-1]

            source_padding_mask = padding_mask(batch.source, dataset.fields["source"])
            target_padding_mask = padding_mask(batch.target, dataset.fields["target"])

            pred = model(
                batch.source, batch.target, source_padding_mask, target_padding_mask
            )

            loss = loss_fn(pred, y)
            loss.backward()
            optimizer.step()
            scheduler.step()

            batches_progress.set_description(
                "[epoch : {}][loss : {:0.4f}]".format(epoch + 1, loss.item())
            )
            losses.append((loss.item(), batch_idx))

        blacklisted_indices = {
            e[1] for e in sorted(losses, key=lambda e: e[0])[:max_blacklisted_len]
        }

        mean_loss = 0
        if len(losses) > 0:
            mean_loss = sum([e[0] for e in losses]) / len(losses)
        tqdm.write(f"mean loss : {mean_loss} ")
        tqdm.write(f"current lr : {scheduler.get_lr()}")

        if epoch % 2 == 0:
            tqdm.write("saving current model...")
            torch.save(model.state_dict(), model_dir + "/model.pth")
            torch.save(dataset.fields["source"], model_dir + "/source_field.pth")
            torch.save(dataset.fields["target"], model_dir + "/target_field.pth")

    return model


if __name__ == "__main__":

    config = Config("configs/default-config.json")
    arg_parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    arg_parser.add_argument("-t", "--train", action="store_true")
    arg_parser.add_argument(
        "-i",
        "--interactive",
        type=str,
        default=None,
        help="If specified, use the given model to launch an interactive translation session.",
    )
    arg_parser.add_argument(
        "-s",
        "--submit",
        type=str,
        default=None,
        help="If specified, will submit using the model in the specified dir",
    )
    arg_parser.add_argument(
        "-cf",
        "--config-file",
        type=str,
        help="""Config file from where config will be loaded, overriding 
                all defaults parameters and arguments specified at command line.
                Except 'default-config.json', all .json file under the 'configs'
                folder are ignored by git to allow for custom configs.
            """,
    )
    arg_parser.add_argument(
        "-bz", "--batch-size", type=int, default=config["batch_size"], help="Batch size"
    )
    arg_parser.add_argument(
        "-msz",
        "--max-seq-size",
        type=int,
        default=config["max_seq_size"],
        help="Sequence size",
    )
    arg_parser.add_argument(
        "-en",
        "--epochs-nb",
        type=int,
        default=config["epochs_nb"],
        help="Epochs number",
    )
    arg_parser.add_argument(
        "-hn",
        "--heads-nb",
        type=int,
        default=config["heads_nb"],
        help="""Number of transformer heads
        (word vectors dimension must be divisible by it)
        (vector dimension is hardcoded at 300 for now)
        """,
    )
    arg_parser.add_argument(
        "-df",
        "--datas-file",
        type=str,
        default=config["datas_file"],
        help="Path to the file containing datas",
    )
    args = arg_parser.parse_args()
    if args.config_file:
        config.load_from_file_(args.config_file)
    else:
        config.update_(vars(args))

    print(f"running with config :\n{str(config)}", file=sys.stderr)

    if args.train:

        training_id = datetime.now().strftime("%Y-%m-%d.%H:%M:%S")
        training_dir = "./out/" + training_id
        print(f"starting training {training_id}", file=sys.stderr)

        train_set, test_set, val_set = load_datasets(
            config["datas_file"], "./datas/cc.en.300.vec", "./datas/cc.zh.300.vec"
        )

        model = Translator(
            train_set.fields["source"].vocab.vectors,
            train_set.fields["target"].vocab.vectors,
            config["max_seq_size"],
            300,  # yes I hardcoded it, what are you gonna do about it ?
            config["heads_nb"],
        )

        os.mkdir(training_dir)

        optimizer = torch.optim.Adam(model.parameters(), lr=8e-5)
        scheduler_step_size_up = 2 * len(train_set)
        print(
            f"[debug] scheduler_step_size_up : {scheduler_step_size_up}",
            file=sys.stderr,
        )
        scheduler = torch.optim.lr_scheduler.CyclicLR(
            optimizer,
            8e-5,
            1e-4,
            cycle_momentum=False,
            step_size_up=scheduler_step_size_up,
        )
        model = train_(
            model,
            train_set,
            torch.device("cuda" if torch.cuda.is_available() else "cpu"),
            torch.nn.CrossEntropyLoss(),
            optimizer,
            scheduler,
            config["epochs_nb"],
            config["batch_size"],
            training_dir,
        )

        torch.save(model.state_dict(), training_dir + "/model.pth")
        torch.save(train_set.fields["source"], training_dir + "/source_field.pth")
        torch.save(train_set.fields["target"], training_dir + "/target_field.pth")
        open(training_dir + "/config.json", "w").write(str(config))

        exit()

    if args.submit:
        model_dir = args.submit.strip("/")
        dataset = load_datasets(
            config["datas_file"],
            "./datas/cc.en.300.vec",
            "./datas/cc.zh.300.vec",
            split=False,
            source_field_path=model_dir + "/source_field.pth",
            target_field_path=model_dir + "/target_field.pth",
        )
        model = Translator(
            dataset.fields["source"].vocab.vectors,
            dataset.fields["target"].vocab.vectors,
            config["max_seq_size"],
            300,
            config["heads_nb"],
        )
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        model.load_state_dict(
            torch.load(model_dir + "/model.pth", map_location=device), strict=False
        )
        with open(config["datas_file"]) as datas_file:
            for line in datas_file:
                input_sent = line.split("\t")[0]
                prediction = predict(
                    torch.tensor(
                        numericalize_en(input_sent, dataset.fields["source"].vocab),
                        dtype=torch.long,
                    ).to(device),
                    model,
                    dataset.fields["source"],
                    dataset.fields["target"],
                    device,
                    2,
                )
                print(input_sent + "\t" + prediction)
        exit()

    if args.interactive:
        model_dir = args.interactive.strip("/")
        dataset = load_datasets(
            config["datas_file"],
            "./datas/cc.en.300.vec",
            "./datas/cc.zh.300.vec",
            split=False,
            source_field_path=model_dir + "/source_field.pth",
            target_field_path=model_dir + "/target_field.pth",
        )
        model = Translator(
            dataset.fields["source"].vocab.vectors,
            dataset.fields["target"].vocab.vectors,
            config["max_seq_size"],
            300,
            config["heads_nb"],
        )
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        model.load_state_dict(
            torch.load(model_dir + "/model.pth", map_location=device), strict=False
        )

        answer = None
        print(
            """Welcome to the interactive mode ! type your english sentence and press
            <ENTER>. type q to quit
            """
        )
        while True:
            answer = input(">")
            if answer == "q":
                break
            print(
                predict(
                    torch.tensor(
                        numericalize_en(answer, dataset.fields["source"].vocab),
                        dtype=torch.long,
                    ).to(device),
                    model,
                    dataset.fields["source"],
                    dataset.fields["target"],
                    device,
                    3,
                )
            )
        exit()

    print("nothing to do. exitting...", file=sys.stderr)

